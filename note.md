1. Як можна створити рядок у JavaScript?
    Є три методи оголошення функції
    - let str1 = 'hello';
    - let str2 = "hello";
    - let str3 = `hello${}`;

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
    Між '' та "" різниці немає, залежить як домовились на продакшені, а зворотні лапки
    дозволяють вставити змінну і розіменувати її.

3. Як перевірити, чи два рядки рівні між собою?
    Порівняти рядки можна методом localCompare()

4. Що повертає Date.now()?
   Date.now()  - повертає timestamp в теперішньому часі

5. Чим відрізняється Date.now() від new Date()?
    Date.now()  - показу timestamp в теперішньому часі
    new Data() - при використанні показує точний час і дату у даний момент без timestamp
    Різниця у виводі інформації
