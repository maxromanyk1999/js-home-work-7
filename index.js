"use strict"

/*
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome,
яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.*/

const str = "А роза упала на лапу Азора.";

const isPalindrome = (str) => {

    let newStr = str.toLowerCase().replace(/[ ,!.\[\]?]/g, '');
    let i = 0;
    let j = newStr.length - 1;

    while (i < j) {
        if (newStr[i] === newStr[j]){
            i++;j--;
        } else {
            return false;
        }
    }
    return true;
}

console.log(isPalindrome(str));

/*2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити,
 максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині,
  і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми.
  Приклади використання функції:
// Рядок коротше 20 символів
    funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false*/


const str1 = "asd wasd qweasd wasd wq";
const validateStringLength = (str, len) => {
    return str.length <= len;
}

console.log(validateStringLength(str1, 22));



/*
3. Створіть функцію, яка визначає скільки повних років користувачу.
Отримайте дату народження користувача через prompt.
Функція повина повертати значення повних років на дату виклику функцію.
*/

/*let userBirthDate = +new Date(prompt("Введіть рік свого народження починаючи з року через пробіл"));

const ageValidator = (age) => {
    let nowDate = + new Date();
    nowDate = nowDate - age;
    return nowDate / 1000 / 60 / 60 / 24 * 0.0027;
}
console.log(ageValidator(userBirthDate));*/

let userBirthday = Date.parse(prompt("Введіть дату народження 'місяць/день/рік' через пробіл"));
const ageValidator = (age) => {
    return age = (new Date() - age) / 1000 / 60 / 60 / 24 / 365;
}
alert(`Тобі повних років ${Math.floor(ageValidator(userBirthday))}`);



/*console.log(Date.now(userBirthDate));
console.log(new Date(userBirthDate));*/

